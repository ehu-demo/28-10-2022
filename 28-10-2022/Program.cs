﻿using static System.Console;

char @continue = 'y';

do
{
    int borderValue;

    while (true)
    {
        Write("Enter border value > 0: ");
        borderValue = int.Parse(ReadLine());
        Clear();
        if (borderValue > 0)
        {
            break;
        }

        WriteLine("Invalid value. Try again.");
    }

    int count;

    while (true)
    {
        Write("Enter count value > 0: ");
        count = int.Parse(ReadLine());
        Clear();
        if (count > 0)
        {
            break;
        }

        WriteLine("Invalid value. Try again.");
    }

    WriteLine();

    const string fizz = "Fizz";
    const string buzz = "Buzz";

    for (int number = 1; number <= borderValue; number++)
    {
        string value = (number % 3) switch
        {
            0 when number % 5 == 0 => $"{fizz}{buzz}",
            0 => fizz,
            _ => number % 5 == 0 ? buzz : $"{number}",
        };

        string separator = (number % count) switch
        {
            0 when number == borderValue => string.Empty,
            0 => $", {Environment.NewLine}",
            _ => number == borderValue ? string.Empty : ", ",
        };

        Write($"{value}{separator}");
    }

    WriteLine();
    WriteLine("Press 'y' or 'Y' if want to continue.");
    @continue = char.Parse(ReadLine());
    Clear();

} while (@continue is 'y' or 'Y');

WriteLine("Finished!");
ReadKey();